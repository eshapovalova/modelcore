﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.WebAPI.Core.DAL.Repository.Base;
using Models.WebAPI.Core.Domain;
using Models.WebAPI.Infrastructure.Domain;
using Models.WebAPI.Infrastructure.Services;

namespace Models.WebAPI.Core.Services
{
    public class SourceService : ISourceService
    {
        private readonly IRepository<Source> _repository;

        public SourceService(IRepository<Source> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<ISource>> GetSources()
        {
            return await _repository.GetAll();
        }

        public async Task<ISource> GetSource(long id)
        {
            return await _repository.Find(id);
        }

        public async Task<long> CreateSource(ISource source)
        {
            var id = await _repository.Create((Source)source);

            return id;
        }

        public async Task UpdateSource(long id, ISource source)
        {
            var item = source as Source;

            await _repository.Update(id, item);
        }

        public async Task DeleteSource(long id)
        {
            await _repository.Delete(id);
        }
    }
}