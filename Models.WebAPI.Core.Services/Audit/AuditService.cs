﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models.WebAPI.Core.DAL;
using Models.WebAPI.Core.Domain.Audit;
using Models.WebAPI.Infrastructure.Domain.Audit;
using Models.WebAPI.Infrastructure.Services.Audit;

namespace Models.WebAPI.Core.Services.Audit
{
    public class AuditService : IAuditService
    {
        private readonly ModuleContext _context;
        private readonly DbSet<AuditMessage> _messages;

        public AuditService(ModuleContext context)
        {
            _context = context;
            _messages = _context.Set<AuditMessage>();
        }

        public async Task SaveRequest(IAuditMessage auditMessage)
        {
            if (auditMessage == null)
                throw new ArgumentException("entity can't be null");

            var entity = auditMessage as AuditMessage;

            if (entity != null)
            {
                await _messages.AddAsync(entity);
                await _context.SaveChangesAsync();
            }
        }
        
        public async Task<IEnumerable<IAuditMessage>> LoadAudits()
        {
            return await _messages.ToListAsync();
        }
    }
}
