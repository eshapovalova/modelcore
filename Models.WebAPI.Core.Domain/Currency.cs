﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.WebAPI.Core.Domain
{
    public class Currency
    {
        [Column("CurrencyId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required]
        public DateTime ETLCreateDate { get; set; }

        [Required]
        public DateTime ETLUpdateDate { get; set; }

        public bool IsDeleted { get; set; }
        [Required]
        public long SourceId { get; set; }

        [Required]
        public string CurrencyKey { get; set; }

        [Required]
        public string CurrencyCode { get; set; }

        [Required]
        [Column("Currency")]
        public string CurrencyName { get; set; }

        public DateTime SourceLastUpdateDate { get; set; }
    }
}
