﻿using Models.WebAPI.Infrastructure.Domain.Audit;

namespace Models.WebAPI.Core.Domain.Audit
{
    public class ApiRequest : IApiRequest
    {
        /// <summary>
        /// Request url (path and parameters)
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// HTTP Method provided in the HTTP Request
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// HTTP Request body data
        /// </summary>
        public string RequestBody { get; set; }
    }
}