﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models.WebAPI.Infrastructure.Domain.Audit;
using Newtonsoft.Json;

namespace Models.WebAPI.Core.Domain.Audit
{
    [Table("AuditMessage")]
    public class AuditMessage : IAuditMessage
    {
        [Key]
        public Guid EventID { get; set; }

        [Required]
        public string EventName { get; set; }

        [Required]
        public string EventType { get; set; }

        public long UserID { get; set; }

        [Required]
        public string IPAddress { get; set; }

        [Required]
        public DateTime EventTime { get; set; }

        [Required]
        public DateTime LogTime { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string RequestData { get; set; }

        [Required]
        public string ResponseData { get; set; }

        [NotMapped]
        public ApiRequest ApiRequest
        {
            get => RequestData != null ? JsonConvert.DeserializeObject<ApiRequest>(RequestData) : null;
            set => RequestData = JsonConvert.SerializeObject(value);
        }
    }
}
