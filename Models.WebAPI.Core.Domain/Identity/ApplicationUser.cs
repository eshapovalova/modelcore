﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Models.WebAPI.Core.Domain.Identity
{
    public class ApplicationUser : IdentityUser { }
}
