﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models.WebAPI.Infrastructure.Domain;

namespace Models.WebAPI.Core.Domain
{
    public class Source : ISource
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("SourceId")]
        public long Id { get; set; }

        [Column("ETLCreateDate")]
        public DateTime CreateDate { get; set; }

        [Column("ETLUpdateDate")]
        public DateTime UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        public string SourceKey { get; set; }
        public long ParentId { get; set; }
    }
}