﻿CREATE TABLE [dbo].[Currencies]
(
	[CurrencyId] BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [SourceId] BIGINT NOT NULL, 
    [CurrencyKey] NVARCHAR(100) NOT NULL, 
    [CurrencyCode] NVARCHAR(50) NOT NULL, 
    [Currency] NVARCHAR(100) NOT NULL, 
    [SourceLastUpdateDate] DATE NOT NULL, 
    [ETLCreateDate] DATE NOT NULL, 
    [ETLUpdateDate] DATE NOT NULL, 
    [IsDeleted] BIT NOT NULL, 
    CONSTRAINT [FK_Currencies_Sources] FOREIGN KEY ([SourceId]) REFERENCES [Sources]([SourceId])
)
