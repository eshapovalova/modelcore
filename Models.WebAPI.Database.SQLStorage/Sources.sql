﻿CREATE TABLE [dbo].[Sources]
(
	[SourceId] BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [SourceKey] NVARCHAR(100) NOT NULL, 
    [ParentId] BIGINT NULL, 
    [ETLCreateDate] DATE NOT NULL, 
    [ETLUpdateDate] DATE NOT NULL, 
    [IsDeleted] BIT NOT NULL    
)
