﻿CREATE TABLE [dbo].[AuditMessage]
(
	[EventID] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [EventName] NVARCHAR(150) NOT NULL, 
    [EventType] NVARCHAR(50) NOT NULL, 
    [UserID] BIGINT NULL, 
    [IPAddress] NVARCHAR(20) NULL, 
    [EventTime] DATETIME NOT NULL, 
    [LogTime] DATETIME NOT NULL, 
    [Status] NVARCHAR(4) NOT NULL, 
    [ResponseData] NVARCHAR(MAX) NOT NULL, 
    [RequestData] NVARCHAR(MAX) NOT NULL
)
