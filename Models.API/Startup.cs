﻿using System.IO;
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Models.API.Core;
using Models.API.Middleware;
using Models.WebAPI.Core.DAL;
using Models.WebAPI.Core.DAL.Repository.Base;
using Models.WebAPI.Core.Domain;
using Models.WebAPI.Core.Domain.Audit;
using Models.WebAPI.Core.Domain.Identity;
using Models.WebAPI.Core.Identity.DAL;
using Models.WebAPI.Core.Services;
using Models.WebAPI.Core.Services.Audit;
using Models.WebAPI.Infrastructure.Domain;
using Models.WebAPI.Infrastructure.Domain.Audit;
using Models.WebAPI.Infrastructure.Services;
using Models.WebAPI.Infrastructure.Services.Audit;
using Swashbuckle.AspNetCore.Swagger;

namespace Models.API
{
    /// <summary>
    /// Startup configuration
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="env">The env.</param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfiguration>(Configuration);

            // Enable CORS:
            //services.AddCors();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder.WithOrigins("http://localhost:5055")
                                      .AllowAnyHeader()
                                      .AllowAnyMethod());
            });
            
            // Adds a default in-memory implementation of IDistributedCache
            services.AddDistributedMemoryCache();
            
            var connectionSettings = Configuration.GetSection("ConnectionStrings:DefaultConnection");
            var connectionString = @"Server=localhost;Database=InsurancesDB;Trusted_Connection=True;";

            if (connectionSettings.Value != null)
            {
                connectionString = connectionSettings.Value;
            }
            
            services.AddDbContext<ModuleContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                // Configure the context to use Microsoft SQL Server.
                options.UseSqlServer(connectionString);
                options.UseOpenIddict();
            });

            // Register the Identity services.
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            });

            services.AddOpenIddict(options =>
            {
                options.AddEntityFrameworkCoreStores<ApplicationDbContext>();
                options.AddMvcBinders();
                options.EnableTokenEndpoint("/connect/token");
                options.AllowPasswordFlow();
                options.AllowRefreshTokenFlow();
                options.DisableHttpsRequirement();
            });

            SetTransient(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Module API", Version = "v1" });
                c.SwaggerDoc("v2", new Info { Title = "Patch implementation", Version = "v2"});

                c.OperationFilter<AuthorizationSwaggerFilter>();
                c.OperationFilter<JsonPatchSwaggerFilter>();

                c.AddSecurityDefinition("Api_OAuth", new OAuth2Scheme
                {
                    Description = "My API client credentials flow",
                    Type = "oauth2",
                    Flow = "password",
                    AuthorizationUrl = Configuration["Authorization:Url"],
                    TokenUrl = Configuration["Authorization:Url"] + "connect/token"
                });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    In = "header"
                });

                // enable XML comments: 
                var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Models.API.xml");
                c.IncludeXmlComments(filePath);
            });

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.InputFormatters.Add(new XmlSerializerInputFormatter());
                options.OutputFormatters.Add(new XmlSerializerOutputFormatter());

                options.Filters.Add(new ProducesAttribute("application/json"));

                options.Conventions.Add(new CommandParameterBindingConvention());
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowSpecificOrigin");

            app.UseOAuthValidation();
            app.UseOpenIddict();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddFile("Logs/myapp-{Date}.txt");

            loggerFactory.AddDebug();
            
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Module API V1");
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "V2 Docs");
                //c.ShowJsonEditor();
                c.ShowRequestHeaders();
            });

            app.UseAuditLogging();
            app.Use(next => context => { context.Request.EnableRewind(); return next(context); });

            app.UseMvc();
        }

        private void SetTransient(IServiceCollection services)
        {
            // Domain entity injection
            services.AddTransient<IAuditMessage, AuditMessage>();
            services.AddTransient<ISource, Source>();

            // Repository injection
            services.AddTransient<IRepository<Source>, Repository<Source>>();

            // Services injection
            services.AddTransient<IAuditService, AuditService>();
            services.AddTransient<ISourceService, SourceService>();
        }
    }
}
