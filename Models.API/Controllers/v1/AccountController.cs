using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.API.ViewModels;
using Models.WebAPI.Core.Domain.Identity;
using Models.WebAPI.Core.Identity.DAL;

namespace Models.API.Controllers.v1
{
    /// <summary>
    /// Account Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/account")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        private static bool _databaseChecked;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="context">The context.</param>
        public AccountController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        /// <summary>
        /// Register new user endpoint
        /// </summary>
        /// <param name="viewModel">The view model which contains the username and password</param>
        /// <response code="409">User has already exists</response>
        /// <response code="200">User has been successfully registered</response>
        /// <response code="400">Bad request</response>
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel viewModel)
        {
            EnsureDatabaseCreated(_context);
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(viewModel.Email);

                if (user != null)
                    return StatusCode(StatusCodes.Status409Conflict);

                user = new ApplicationUser {UserName = viewModel.Email, Email = viewModel.Email};

                var result = await _userManager.CreateAsync(user, viewModel.Password);

                if (result.Succeeded)
                    return Ok($"{user.Email} has been successfully registered");

                AddErrors(result);
            }

            return BadRequest(ModelState);
        }

        #region Helpers

        /// <summary>
        /// Ensures the database created.
        /// </summary>
        /// <param name="context">The context.</param>
        private static void EnsureDatabaseCreated(ApplicationDbContext context)
        {
            if (!_databaseChecked)
            {
                _databaseChecked = true;
                context.Database.EnsureCreated();
            }
        }

        /// <summary>
        /// Adds the errors.
        /// </summary>
        /// <param name="result">The result.</param>
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion
    }
}