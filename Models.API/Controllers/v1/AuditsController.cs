using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.WebAPI.Infrastructure.Services.Audit;

namespace Models.API.Controllers.v1
{
    /// <summary>
    /// Audits Controller
    /// </summary>
    [Produces("application/json")]
    [Route("api/audits")]
    public class AuditsController : Controller
    {
        private readonly IAuditService _service;

        /// <summary>
        /// Controller which represents API for Audits 
        /// </summary>
        /// <param name="service"></param>
        public AuditsController(IAuditService service)
        {
            _service = service;
        }

        /// <summary>
        /// Retrieves information about http audits
        /// </summary>
        /// <remarks>Test comment</remarks>
        /// <response code="401">Unauthorize access</response>
        /// <response code="500">Server exception</response>
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize]
        [HttpGet(Name = "Get[controller]")]
        public IActionResult Get()
        {
            var items = _service.LoadAudits();

            return Ok(items);
        }
    }
}