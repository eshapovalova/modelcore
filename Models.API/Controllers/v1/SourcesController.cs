using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Models.WebAPI.Core.Domain;
using Models.WebAPI.Infrastructure.Services;
using Microsoft.Extensions.Configuration;

namespace Models.API.Controllers.v1
{
    /// <summary>
    /// Sources API controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/sources")]
    public class SourcesController : Controller
    {
        private readonly ISourceService _service;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="SourcesController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="configuration">The configuration.</param>
        public SourcesController(ISourceService service, IConfiguration configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var items = await _service.GetSources();

            return Ok(items);
        }

        /// <summary>
        /// Get source by id
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <response code="401">Unauthorize access</response>
        /// <response code="500">Server exception</response>
        /// <response code="200">Request finish</response>
        [ProducesResponseType(typeof(Source), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}", Name = "GetSources")]
        public async Task<Source> Get([FromQuery] long id)
        {
            var item = await _service.GetSource(id) as Source;

            return item;
        }

        /// <summary>
        /// Posts the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        [HttpPost(Name = "CreateSource")]
        public async Task<long> Post([FromBody]Source source)
        {
            var id = await _service.CreateSource(source);

            return id;
        }

        /// <summary>
        /// Patches the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="patchDoc">The patch document.</param>
        /// <returns></returns>
        [HttpPatch("{id}", Name = "UpdateSource")]
        [ApiExplorerSettings(GroupName = "v2")]
        public async Task<IActionResult> Patch(long id, [FromBody]JsonPatchDocument<Source> patchDoc)
        {
            var fromDb = await _service.GetSource(id) as Source;
            patchDoc.ApplyTo(fromDb);
            await _service.UpdateSource(id, fromDb);

            return Ok(fromDb);
        }

        /// <summary>
        /// Puts the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        [HttpPut(Name = "UpdateSourceItem")]
        public async Task<IActionResult> Put(Source source)
        {
            await _service.UpdateSource(source.Id, source);

            return Ok(source);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete(Name="DeleteSourceItem")]
        public async Task<IActionResult> Delete([FromBody] long id)
        {
            await _service.DeleteSource(id);
            return Ok();
        }
    }
}