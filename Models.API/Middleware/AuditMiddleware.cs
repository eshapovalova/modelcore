﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Models.WebAPI.Core.Common.Constants;
using Models.WebAPI.Core.Domain.Audit;
using Models.WebAPI.Infrastructure.Services.Audit;

namespace Models.API.Middleware
{
    public class AuditMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IAuditService _service;

        public AuditMiddleware(RequestDelegate next, IAuditService service)
        {
            _next = next;
            _service = service;
        }

        public async Task Invoke(HttpContext context)
        {
            var contextFeatureHttp = context.Features.Get<IHttpRequestFeature>();

            if (contextFeatureHttp.Path.Contains("/api/") 
                && contextFeatureHttp.Method == HttpMethods.Post 
                && contextFeatureHttp.Method == HttpMethods.Patch
                && contextFeatureHttp.Method == HttpMethods.Put
                && contextFeatureHttp.Method == HttpMethods.Delete)
            {
                var injectedRequestStream = new MemoryStream();
                var injectedResponseStream = new MemoryStream();

                AuditMessage audit;
                try
                {
                    audit = await PrepareAuditRequestMessage(context, contextFeatureHttp);

                    var requestLog = $"REQUEST HttpMethod: {context.Request.Method}, Path: {context.Request.Path}";

                    using (var bodyReader = new StreamReader(context.Request.Body))
                    {
                        var bodyAsText = bodyReader.ReadToEnd();

                        if (string.IsNullOrWhiteSpace(bodyAsText) == false)
                        {
                            requestLog += $", Body: {bodyAsText}";
                        }

                        audit.ApiRequest = new ApiRequest
                        {
                            Url = contextFeatureHttp.Path,
                            Method = contextFeatureHttp.Method,
                            RequestBody = bodyAsText
                        };

                        //audit.ApiRequest.RequestBody = bodyAsText;

                        var bytesToWrite = Encoding.UTF8.GetBytes(bodyAsText);
                        injectedRequestStream.Write(bytesToWrite, 0, bytesToWrite.Length);
                        injectedRequestStream.Seek(0, SeekOrigin.Begin);
                        context.Request.Body = injectedRequestStream;
                    }

                    var originalBodyStream = context.Response.Body;

                    using (var responseBody = new MemoryStream())
                    {
                        //context.Response.Body = responseBody;

                        await _next(context);

                        //audit.ResponseData = await FormatResponse(context.Response);
                        //await responseBody.CopyToAsync(originalBodyStream);
                    }

                    audit.ResponseData = string.Empty;
                    audit.Status = context.Response.StatusCode.ToString();
                    audit.EventType = context.Response.StatusCode != StatusCodes.Status200OK ? ApplicationEventType.DataError : ApplicationEventType.DataChange;
                }
                finally
                {
                    injectedRequestStream.Dispose();
                }

                await _service.SaveRequest(audit);
            }
            else
            {
                await _next(context);
            }
        }

        private async Task<AuditMessage> PrepareAuditRequestMessage(HttpContext context, IHttpRequestFeature contextFeatureHttp)
        {
            var auditRequestMessage = new AuditMessage
            {
                EventID = Guid.NewGuid(),
                EventName = $"{contextFeatureHttp.Path} - {contextFeatureHttp.Method}",
                IPAddress = $"{context.Connection.RemoteIpAddress} {context.Connection.RemotePort}",
                EventTime = DateTime.Now,
                LogTime = DateTime.Now
            };
            return auditRequestMessage;
        }

        private async Task<string> FormatResponse(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return $"Response {text}";
        }
    }

    public class ResponseResult
    {
        public string EventType { get; set; }
        public string ResponseBody { get; set; }
    }
}
