                                                          
$global:p = @();                                  
                                                          
function global:Find-ChildProcess {
  param($ID=$PID)

  $result = Get-CimInstance win32_process | 
    where { $_.ParentProcessId -eq $ID } 
    select -Property ProcessId 

  $result
  $result | 
    Where-Object { $_.ProcessId -ne $null } | 
    ForEach-Object {
      Find-ChildProcess -id $_.ProcessId
    }
}

function global:Kill-Demo {
  $Global:p | 
    foreach { Find-ChildProcess -ID $_.Id } | 
    foreach { kill -id $_.ProcessId }
}

[System.Environment]::SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");

# Angular Application
Push-Location "./AngularApp"
npm install -y
$global:p += Start-Process npm -ArgumentList "run start" -PassThru
Pop-Location

Start-Process -FilePath http://localhost:5055