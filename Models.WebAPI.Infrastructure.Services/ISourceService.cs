﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.WebAPI.Infrastructure.Domain;

namespace Models.WebAPI.Infrastructure.Services
{
    public interface ISourceService
    {
        Task<IEnumerable<ISource>> GetSources();

        Task<ISource> GetSource(long id);

        Task<long> CreateSource(ISource source);

        Task UpdateSource(long id, ISource source);

        Task DeleteSource(long id);
    }
}