﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.WebAPI.Infrastructure.Domain.Audit;

namespace Models.WebAPI.Infrastructure.Services.Audit
{
    public interface IAuditService
    {
        Task SaveRequest(IAuditMessage auditMessage);
        
        Task<IEnumerable<IAuditMessage>> LoadAudits();
    }
}