﻿
namespace Models.WebAPI.Core.Common.Constants
{
    public class ApplicationEventType
    {
        public const string DataChange = "Data Change";
        public const string Logout = "Logout";
        public const string Unauthorized = "Unauthorized Access";
        public const string DataError = "Data Error";
        public const string PrivateInfo = "Private Information";
        public const string Information = "Information";
    }
}
