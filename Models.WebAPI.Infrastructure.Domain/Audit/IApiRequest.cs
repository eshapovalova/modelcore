﻿namespace Models.WebAPI.Infrastructure.Domain.Audit
{
    public interface IApiRequest
    {
        string Url { get; set; }

        string Method { get; set; }

        string RequestBody { get; set; }
    }
}