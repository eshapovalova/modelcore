﻿using System;

namespace Models.WebAPI.Infrastructure.Domain.Audit
{
    public interface IAuditMessage
    {
        Guid EventID { get; set; }
        string EventName { get; set; }
        string EventType { get; set; }
        long UserID { get; set; }
        string IPAddress { get; set; }
        DateTime EventTime { get; set; }
        DateTime LogTime { get; set; }
        string Status { get; set; }
        string RequestData { get; set; }
        string ResponseData { get; set; }
    }
}