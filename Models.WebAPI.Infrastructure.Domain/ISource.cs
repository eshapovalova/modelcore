﻿using Models.WebAPI.Infrastructure.Domain.Base;

namespace Models.WebAPI.Infrastructure.Domain
{
    public interface ISource : IEntity
    {
        string SourceKey { get; set; }

        long ParentId { get; set; }
    }
}