﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.WebAPI.Infrastructure.Domain.Base
{
    public interface IEntity
    {
        long Id { get; set; }
        
        DateTime CreateDate { get; set; }
        
        DateTime UpdateDate { get; set; }

        bool IsDeleted { get; set; }
    }
}