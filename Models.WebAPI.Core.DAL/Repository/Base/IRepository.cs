﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Models.WebAPI.Infrastructure.Domain.Base;

namespace Models.WebAPI.Core.DAL.Repository.Base
{
    public interface IRepository<T> where T : class
    {
        Task<IQueryable<T>> GetAll();

        Task<IQueryable<T>> FindBy(Expression<Func<T, bool>> predicate);

        Task<T> Find(long id);

        Task<long> Create(T entity);

        Task Delete(long id);

        Task Update(long id, T newValues);
        
    }
}
