﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models.WebAPI.Infrastructure.Domain.Base;

namespace Models.WebAPI.Core.DAL.Repository.Base
{
    public class Repository<T> : IRepository<T> where T : class 
    {
        private readonly ModuleContext _context;
        private readonly DbSet<T> _entities;

        public Repository(ModuleContext context)
        {
            _context = context;
            _entities = _context.Set<T>();
        }

        public async Task<IQueryable<T>> GetAll()
        {
            return await Task.FromResult(_entities.AsQueryable());
        }

        public async Task<IQueryable<T>> FindBy(Expression<Func<T, bool>> predicate)
        {
            var items = _entities.Where(predicate).AsQueryable();

            return await Task.FromResult(items);
        }

        public async Task<T> Find(long id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task<long> Create(T entity)
        {
            if (entity == null)
                throw new ArgumentException("entity can't be null");

            await _entities.AddAsync(entity);

            AddTimestamps();
            await _context.SaveChangesAsync();

            var result = entity as IEntity;

            return result?.Id ?? 0;
        }

        public async Task Delete(long id)
        {
            var entity = await _context.FindAsync(typeof(T), id);

            if (entity == null)
                throw new ArgumentException("entity can't be null");

            _entities.Remove((T)entity);

            AddTimestamps();
            await _context.SaveChangesAsync();
        }

        public async Task Update(long id, T newValues)
        {
            var oldEntity = await _context.FindAsync(typeof(T), id);

            if (oldEntity == null)
                throw new ArgumentException("entity can't be null");

            _context.Entry(oldEntity).CurrentValues.SetValues(newValues);

            AddTimestamps();
            await _context.SaveChangesAsync();
        }

        private void AddTimestamps()
        {
            var entities = _context.ChangeTracker.Entries().Where(x => x.Entity is IEntity && (x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted));
            
            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((IEntity)entity.Entity).CreateDate = DateTime.UtcNow;
                }

                if (entity.State == EntityState.Deleted)
                {
                    ((IEntity) entity.Entity).IsDeleted = true;
                }

                ((IEntity)entity.Entity).UpdateDate = DateTime.UtcNow;
            }
        }
    }
}