﻿using Microsoft.EntityFrameworkCore;
using Models.WebAPI.Core.Domain;
using Models.WebAPI.Core.Domain.Audit;

namespace Models.WebAPI.Core.DAL
{
    public class ModuleContext : DbContext
    {
        public ModuleContext(DbContextOptions<ModuleContext> options)
            : base(options)
        { }

        public DbSet<Currency> Currencies { get; set; }

        public DbSet<Source> Sources { get; set; }

        public DbSet<AuditMessage> AuditMessages { get; set; }
    }
}
